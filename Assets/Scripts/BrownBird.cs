﻿using System.Collections;
using UnityEngine;

public class BrownBird : Bird
{
    [SerializeField]
    public float impact;
    public float force;
    public LayerMask layerhit;
    public bool _hasExplosive = false;
    public void Explosive()
    {

        if (!_hasExplosive)
        {
            Collider2D[] objects = Physics2D.OverlapCircleAll(transform.position, impact, layerhit);
            foreach (Collider2D obj in objects)
            {
                Vector2 direction = obj.transform.position - transform.position;
                obj.GetComponent<Rigidbody2D>().AddForce(direction * force);

            }
            _hasExplosive = true;
        }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {

        if(collision.gameObject)
        {
            StartCoroutine(ExplosiveAfter(1));
        }
    }

    private IEnumerator ExplosiveAfter(float second)
    {
        yield return new WaitForSeconds(second);
        Explosive();
    }
}